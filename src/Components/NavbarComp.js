import React, { Component } from 'react'
import {Navbar, Nav, NavDropdown, Form, FormControl, Button} from 'react-bootstrap';
//import { Switch, Redirect } from 'react-router-dom';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";

import Home from "./home";
import About from "./about";
import Contact from "./contact";

export default class NavbarComp extends Component {
    render() {
        return (
            <Router>
        <div>
            <Navbar bg="dark" variant = {"dark"} expand="lg">
             
                <Navbar.Brand href="#home">Casa Brossom</Navbar.Brand>
                <Navbar.Toggle aria-controls="basic-navbar-nav" />
                <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="me-auto">
                <Nav.Link href="home">Home</Nav.Link>
                <Nav.Link href="about">About Us</Nav.Link>
                <Nav.Link href="contac">Contact Us</Nav.Link>
                <NavDropdown title="Dropdown" id="basic-nav-dropdown">
                <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                <NavDropdown.Item href="#action/3.2"> Another action </NavDropdown.Item>
                <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item href="#action/3.4"> Separated link </NavDropdown.Item>
                </NavDropdown>
                </Nav>
                <Form inline> 
                    <FormControl type="text" placeholder = "Search" className= "mr-sm-2" />
                    <Button variant ="ourline-success"> Search </ Button>
                </Form>
                </Navbar.Collapse>
            </Navbar>
        </div>
        
        <div>
                    <Switch>
                        <Route path="/about">
                            <About />
                        </Route>
                        <Route path="/contact">
                            <Contact />
                        </Route>
                        <Route path="/">
                            <Home />
                        </Route>
                    </Switch>
                </div>

            </Router>
        )
    }
}